package domain

import (
	"context"

	"gorm.io/gorm"
)

// Author representing the Author data struct
type Author struct {
	gorm.Model
	ID   int64  `json:"id" gorm:"primaryKey"`
	Name string `json:"name"`
}

// AuthorRepository represent the author's repository contract
type AuthorRepository interface {
	GetByID(ctx context.Context, id int64) (Author, error)
}
