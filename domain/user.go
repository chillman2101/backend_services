package domain

import (
	"context"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID       int64  `json:"id" gorm:"primaryKey"`
	Name     string `json:"name"`
	Username string `gorm:"unique" json:"username"`
	Password string `json:"password"`
	Email    string `gorm:"unique" json:"email"`
}

// ArticleUsecase represent the article's usecases
//
//go:generate mockery --name UserUsecase
type UserUsecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]User, string, error)
	GetByID(ctx context.Context, id int64) (User, error)
	Update(ctx context.Context, ar *User) error
	GetByTitle(ctx context.Context, title string) (User, error)
	Store(context.Context, *User) error
	Delete(ctx context.Context, id int64) error
}

// UserRepository represent the User's repository contract
//
//go:generate mockery --name UserRepository
type UserRepository interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []User, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (User, error)
	GetByTitle(ctx context.Context, title string) (User, error)
	Update(ctx context.Context, ar *User) error
	Store(ctx context.Context, a *User) error
	Delete(ctx context.Context, id int64) error
}
