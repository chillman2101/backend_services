package domain

import (
	"context"

	"gorm.io/gorm"
)

// Article is representing the Article data struct
type Article struct {
	gorm.Model
	ID       int64  `json:"id" gorm:"primaryKey"`
	Title    string `json:"title" `
	Content  string `json:"content" `
	AuthorId int64  `json:"author_id"`
	Author   Author `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

// ArticleUsecase represent the article's usecases
type ArticleUsecase interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]Article, string, error)
	GetByID(ctx context.Context, id int64) (Article, error)
	Update(ctx context.Context, ar *Article) error
	GetByTitle(ctx context.Context, title string) (Article, error)
	Store(context.Context, *Article) error
	Delete(ctx context.Context, id int64) error
}

// ArticleRepository represent the article's repository contract
type ArticleRepository interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []Article, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (Article, error)
	GetByTitle(ctx context.Context, title string) (Article, error)
	Update(ctx context.Context, ar *Article) error
	Store(ctx context.Context, a *Article) error
	Delete(ctx context.Context, id int64) error
}
