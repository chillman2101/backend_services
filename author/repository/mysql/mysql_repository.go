package mysql

import (
	"context"

	"github.com/bxcodec/go-clean-arch/domain"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type mysqlAuthorRepo struct {
	DB *gorm.DB
}

// NewMysqlAuthorRepository will create an implementation of author.Repository
func NewMysqlAuthorRepository(db *gorm.DB) domain.AuthorRepository {
	return &mysqlAuthorRepo{
		DB: db,
	}
}

func (m *mysqlAuthorRepo) getOne(ctx context.Context, query string, args ...interface{}) (res domain.Author, err error) {
	res = domain.Author{}
	if err := m.DB.Raw(query, args...).Scan(&res); err.Error != nil {
		logrus.Error(err)
		return domain.Author{}, err.Error
	}
	return
}

func (m *mysqlAuthorRepo) GetByID(ctx context.Context, id int64) (domain.Author, error) {
	query := `SELECT id, name, created_at, updated_at FROM authors WHERE id=?`
	return m.getOne(ctx, query, id)
}
