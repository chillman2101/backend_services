package usecase

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	"github.com/bxcodec/go-clean-arch/domain"
)

type UserUsecase struct {
	UserRepo       domain.UserRepository
	contextTimeout time.Duration
}

// NewUserUsecase will create new an UserUsecase object representation of domain.UserUsecase interface
func NewUserUsecase(a domain.UserRepository, timeout time.Duration) domain.UserUsecase {
	return &UserUsecase{
		UserRepo:       a,
		contextTimeout: timeout,
	}
}

/*
* In this function below, I'm using errgroup with the pipeline pattern
* Look how this works in this package explanation
* in godoc: https://godoc.org/golang.org/x/sync/errgroup#ex-Group--Pipeline
 */
func (a *UserUsecase) fillUserDetails(c context.Context, data []domain.User) ([]domain.User, error) {
	g, ctx := errgroup.WithContext(c)

	// Get the author's id
	mapUsers := map[int64]domain.User{}

	for _, User := range data { //nolint
		mapUsers[User.ID] = domain.User{}
	}
	// Using goroutine to fetch the author's detail
	chanUser := make(chan domain.User)
	for userID := range mapUsers {
		userID := userID
		g.Go(func() error {
			res, err := a.UserRepo.GetByID(ctx, userID)
			if err != nil {
				return err
			}
			chanUser <- res
			return nil
		})
	}

	go func() {
		err := g.Wait()
		if err != nil {
			logrus.Error(err)
			return
		}
		close(chanUser)
	}()

	for user := range chanUser {
		if user != (domain.User{}) {
			mapUsers[user.ID] = user
		}
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	// merge the author's data
	for index, item := range data { //nolint
		if a, ok := mapUsers[item.ID]; ok {
			data[index] = a
		}
	}
	return data, nil
}

func (a *UserUsecase) Fetch(c context.Context, cursor string, num int64) (res []domain.User, nextCursor string, err error) {
	if num == 0 {
		num = 10
	}

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	res, nextCursor, err = a.UserRepo.Fetch(ctx, cursor, num)
	if err != nil {
		return nil, "", err
	}

	// res, err = a.fillUserDetails(ctx, res)
	// if err != nil {
	// 	nextCursor = ""
	// }
	return
}

func (a *UserUsecase) GetByID(c context.Context, id int64) (res domain.User, err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	res, err = a.UserRepo.GetByID(ctx, id)
	if err != nil {
		return
	}

	// resUser, err := a.UserRepo.GetByID(ctx, res.ID)
	// if err != nil {
	// 	return domain.User{}, err
	// }
	// res = resUser
	return
}

func (a *UserUsecase) Update(c context.Context, ar *domain.User) (err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	ar.UpdatedAt = time.Now()
	return a.UserRepo.Update(ctx, ar)
}

func (a *UserUsecase) GetByTitle(c context.Context, title string) (res domain.User, err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	res, err = a.UserRepo.GetByTitle(ctx, title)
	if err != nil {
		return
	}

	// resUser, err := a.UserRepo.GetByID(ctx, res.ID)
	// if err != nil {
	// 	return domain.User{}, err
	// }

	// res = resUser
	return
}

func (a *UserUsecase) Store(c context.Context, m *domain.User) (err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	existedUser, _ := a.GetByTitle(ctx, m.Username)
	if existedUser != (domain.User{}) {
		return domain.ErrConflict
	}

	err = a.UserRepo.Store(ctx, m)
	return
}

func (a *UserUsecase) Delete(c context.Context, id int64) (err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	existedUser, err := a.UserRepo.GetByID(ctx, id)
	if err != nil {
		return
	}
	if existedUser == (domain.User{}) {
		return domain.ErrNotFound
	}
	return a.UserRepo.Delete(ctx, id)
}
