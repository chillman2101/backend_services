package mysql

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"

	"github.com/bxcodec/go-clean-arch/domain"
	"github.com/bxcodec/go-clean-arch/user/repository"
)

type mysqlUserRepository struct {
	Conn *gorm.DB
}

// NewMysqlUserRepository will create an object that represent the User.Repository interface
func NewMysqlUserRepository(conn *gorm.DB) domain.UserRepository {
	return &mysqlUserRepository{conn}
}

func (m *mysqlUserRepository) fetch(ctx context.Context, query string, args ...interface{}) (result []domain.User, err error) {
	rows, err := m.Conn.WithContext(ctx).Raw(query, args...).Rows()
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	defer func() {
		errRow := rows.Close()
		if errRow != nil {
			logrus.Error(errRow)
		}
	}()

	result = make([]domain.User, 0)
	for rows.Next() {
		t := domain.User{}
		authorID := int64(0)
		err = rows.Scan(
			&t.ID,
			&t.Username,
			&t.Name,
			&authorID,
			&t.UpdatedAt,
			&t.CreatedAt,
		)

		if err != nil {
			logrus.Error(err)
			return nil, err
		}
		result = append(result, t)
	}

	return result, nil
}

func (m *mysqlUserRepository) Fetch(ctx context.Context, cursor string, num int64) (res []domain.User, nextCursor string, err error) {
	query := `SELECT id,title,content, updated_at, created_at
  						FROM Users WHERE created_at > ? ORDER BY created_at LIMIT ? `

	decodedCursor, err := repository.DecodeCursor(cursor)
	if err != nil && cursor != "" {
		return nil, "", domain.ErrBadParamInput
	}

	res, err = m.fetch(ctx, query, decodedCursor, num)
	if err != nil {
		return nil, "", err
	}

	if len(res) == int(num) {
		nextCursor = repository.EncodeCursor(res[len(res)-1].CreatedAt)
	}

	return
}
func (m *mysqlUserRepository) GetByID(ctx context.Context, id int64) (res domain.User, err error) {
	var list []domain.User
	m.Conn.Where("id = ?", id).Find(&list)
	if len(list) > 0 {
		res = list[0]
	} else {
		return res, domain.ErrNotFound
	}

	return
}

func (m *mysqlUserRepository) GetByTitle(ctx context.Context, title string) (res domain.User, err error) {
	var list []domain.User
	m.Conn.Where("username = ?", title).Find(&list)

	if len(list) > 0 {
		res = list[0]
	} else {
		return res, domain.ErrNotFound
	}
	return
}

func (m *mysqlUserRepository) Store(ctx context.Context, a *domain.User) (err error) {
	m.Conn.Preload("Author").Create(&a).First(&a)
	return
}

func (m *mysqlUserRepository) Delete(ctx context.Context, id int64) (err error) {
	query := "DELETE FROM Users WHERE id = ?"

	stmt, err := m.Conn.ConnPool.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return
	}

	rowsAfected, err := res.RowsAffected()
	if err != nil {
		return
	}

	if rowsAfected != 1 {
		err = fmt.Errorf("weird  Behavior. Total Affected: %d", rowsAfected)
		return
	}

	return
}
func (m *mysqlUserRepository) Update(ctx context.Context, ar *domain.User) (err error) {
	query := `UPDATE Users set title=?, content=?, updated_at=? WHERE ID = ?`

	stmt, err := m.Conn.ConnPool.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, ar.Title, ar.Content, ar.Author.ID, ar.UpdatedAt, ar.ID)
	if err != nil {
		return
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return
	}
	if affect != 1 {
		err = fmt.Errorf("weird  Behavior. Total Affected: %d", affect)
		return
	}

	return
}
