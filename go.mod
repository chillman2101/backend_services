module github.com/bxcodec/go-clean-arch

go 1.12

require (
	github.com/bxcodec/faker v1.4.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-sql-driver/mysql v1.3.0
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/labstack/echo v3.3.5+incompatible
	github.com/labstack/gommon v0.0.0-20180426014445-588f4e8bddc6 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/pelletier/go-toml/v2 v2.0.2 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/afero v1.9.2 // indirect
	github.com/spf13/viper v1.12.0
	github.com/stretchr/testify v1.8.4
	github.com/subosito/gotenv v1.4.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sync v0.5.0
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.15.0
	gopkg.in/ini.v1 v1.66.6 // indirect
	gorm.io/driver/postgres v1.5.4
	gorm.io/gorm v1.25.5
)
